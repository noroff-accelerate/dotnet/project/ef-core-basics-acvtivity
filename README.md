# EF Core Basics Activity - Solution

This repository contains the completed solution for the EF Core Basics Activity, aimed at teaching the fundamentals of Entity Framework Core in a .NET 6 console application.

## Overview

The solution includes:
- Entity Framework Core setup in a .NET console application.
- DbContext configuration for database interactions.
- Model class creation and configuration within the `Data` directory.
- Console logging implementation for EF Core operations.
- Database schema management through migrations.
- Model updates and migration application.
- Migration rollback procedures.

## Getting Started

**To run the solution:**

1. Clone the repository:

```
git clone <repository-url>
```

2. Navigate to the solution directory:

```
cd <solution-directory>
```

3. Build and run the project:

```
dotnet build
dotnet run
```


## Structure

- **Data**: Contains the `DbContext`, configuration, and model classes.
- **Migrations**: Holds generated migrations for database schema changes.
- **Program.cs**: The entry point for demonstrating EF Core operations.

## License

This solution is under the MIT License. See [LICENSE](LICENSE.txt) for details.

## Feedback

For questions or feedback, please open an issue in the repository.
