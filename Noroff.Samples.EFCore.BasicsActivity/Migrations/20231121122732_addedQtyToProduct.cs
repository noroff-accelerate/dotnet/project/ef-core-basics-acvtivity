﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Noroff.Samples.EFCore.BasicsActivity.Migrations
{
    /// <inheritdoc />
    public partial class addedQtyToProduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QuantityOnHand",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QuantityOnHand",
                table: "Products");
        }
    }
}
